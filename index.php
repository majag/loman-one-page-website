<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Loman d.o.o - uzgoj i prodaja peradi</title>
		<meta charset="utf-8"/>	    
	    <!-- Meta tags -->
	    <meta name="description" content="Uzgoj i prodaja peradi" />
	    <meta name="keywords" content="Loman, uzgoj, prodaja, peradi, pilići, valjenje, jaja, Pleternica" />
	    <meta name="viewport" content="initial-scale=1.0">
    	<!-- Favicon -->
    	[if lte IE 8]><link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />[endif]
    	<link rel="icon" href="images/favicon.ico" type="image/png" /> 

	    <!-- CSS -->
	    <link rel="stylesheet" href="assets/css/style.css" type="text/css"/>
	    <!--FONTOVI-->
	    <link rel="stylesheet" href="assets/fonts/webfontkit/stylesheet.css" type="text/css"/>
	    <!-- CSS ZA SLIDER-->
	    <link rel="stylesheet" type="text/css" href="assets/css/demo.css" />        
        <link rel="stylesheet" type="text/css" href="assets/css/custom.css" />
        <!--CSS ZA KALENDAR-->
        <link rel="stylesheet" type="text/css" href="assets/css/jquery-ui-1.10.3.custom.css" />

    	<!-- JavaScript -->
	    <script src="assets/js/jquery-1.10.0.min.js"></script>
	    <!-- SMOTH SCROLL skripte-->	    
	    <script src="assets/js/smooth-scroll/jquery-nav.js"></script>
	    <script src="assets/js/smooth-scroll/jquery-scrollTo.js"></script>
	    <!-- KALENDAR SKRIPTE-->
	    <script type="text/javascript" src="assets/js/kalendari/jquery-ui-1.10.3.custom.js"></script>
	    <script type="text/javascript" src="assets/js/kalendari/kalendari.js"></script>
	    <!--MAIN skripta s pozivima funkcija-->
	    <script src="assets/js/main.js"></script> <!-- ovdje se stavis sve funkcije sajta -->
	    <!--SLIDER skripte -->
	    <script type="text/javascript" src="assets/js/slider/modernizr.custom.79639.js"></script>
	    <script type="text/javascript" src="assets/js/slider/jquery.ba-cond.min.js"></script>
		<script type="text/javascript" src="assets/js/slider/jquery.slitslider.js"></script>
		<!-- GOOGLE MAPS API-->
		<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCdqUIbisqH3c1-rzzkHdYnYtHEAj_T-t4&sensor=false"></script>
		<script type="text/javascript" src="assets/js/google-map.js"></script>    		
	    <!--IE HTML5-->
    	[if IE]><script type="text/javascript" src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>[endif]
    	
	</head>

	<body>
		<header id="main-header">
		<div class="wrap">

			<a href="#galerija" id="logo" title="Loman d.o.o">
				<img src="images/logo/logo5.png" alt="Loman d.o.o - Galerija">	
			</a>

			<nav>				
				<ul id="nav">					
				<li><a href="#o-nama" title="O nama" class="button">O nama</a></li>
				<li><a href="#informacije" title="Informacije" class="button">Informacije</a></li>
				<li><a href="#rezervacija" title="Rezervacija" class="button">Rezervacija</a></li>
				<li><a href="#kontakt" titlt="Kontakt" class="button">Kontakt</a></li>
				</ul>
			</nav>

		</div>
		</header>
		
		<div id="content" class="wrap">

			<div id="galerija" class="gallery">

				<div class="container demo-2">			

		            <div id="slider" class="sl-slider-wrapper">

						<div class="sl-slider">
						
							<div class="sl-slide" data-orientation="horizontal" data-slice1-rotation="-25" data-slice2-rotation="-25" data-slice1-scale="2" data-slice2-scale="2">
								<div class="sl-slide-inner">
									<div class="bg-img bg-img-1"></div>									
								</div>
							</div>
							
							<div class="sl-slide" data-orientation="vertical" data-slice1-rotation="10" data-slice2-rotation="-15" data-slice1-scale="1.5" data-slice2-scale="1.5">
								<div class="sl-slide-inner">
									<div class="bg-img bg-img-2"></div>									
								</div>
							</div>
							
							<div class="sl-slide" data-orientation="horizontal" data-slice1-rotation="3" data-slice2-rotation="3" data-slice1-scale="2" data-slice2-scale="1">
								<div class="sl-slide-inner">
									<div class="bg-img bg-img-3"></div>									
								</div>
							</div>
							
							<div class="sl-slide" data-orientation="vertical" data-slice1-rotation="-5" data-slice2-rotation="25" data-slice1-scale="2" data-slice2-scale="1">
								<div class="sl-slide-inner">
									<div class="bg-img bg-img-4"></div>								
								</div>
							</div>
							
							<div class="sl-slide" data-orientation="horizontal" data-slice1-rotation="-5" data-slice2-rotation="10" data-slice1-scale="2" data-slice2-scale="1">
								<div class="sl-slide-inner">
									<div class="bg-img bg-img-5"></div>
									
								</div>
							</div>
						</div><!-- /sl-slider -->

						<nav id="nav-dots" class="nav-dots">
							<span class="nav-dot-current"></span>
							<span></span>
							<span></span>
							<span></span>
							<span></span>
						</nav>

					</div><!-- /slider-wrapper -->			

		        </div>				
			</div>
			
			<div id="o-nama" class="content">
				<div id="o-nama-txt">
				<h1>Uzgoj i prodaja peradi</h1>
				<h2>Loman d.o.o.</h2>
				<p>Privatno poduzeće iz Pleternice koje se bavi uzgojem i prodajom petodnevnih pilića.<br /> 
				Krenuli smo sa radom 1994. godine te do danas ostali među jedinima na prostoru Požeško-slavonske županije.<br /> 
				Prodajemo isključivo petodnevne piliće pasmine Cobb 700 (bijele kokoši).<br /> 
				Pilići primaju sve potrebne vitamine i  minerale koji su im potrebni u prvih 5 dana.<br />
				Piliće dostavljamo na vašu adresu svakog utorka. Ako imate svoja jaja koja bi željeli leći, u za to predviđenim valionicima, mozeteih donijeti kod nas.<br /> 
				Također ležemo i pure.<p>
				</div>
			</div>

			<div id="informacije" class="content float">

				<div id="savjeti">
					<h1>Savjeti</h1><br /><br />
					<p>Jaja koja donosite nebi smjela biti starija od 10 dana (jaja starija od 10 dana imaju veliku šansu za jako slabu valjivost za koju mi ne odgovaramo)<br /><br />
					Jaja je najbolje čuvati u hladnijem prostoru ali ne ispod 15°C (ne u hladnjaku)<br /><br />
					Prljava jaja nemojte prati u vodi, to također smanjuje valjivost<br /><br />
					Jaja očistiti laganim struganjem<br /><br /> </p>
				</div>

				<div id="cijene" class="float">
					<h1>Cijene</h1><br />

					<div id="valjenje">
						<h3>Valjenje</h3><br />
						<p><b>1 kn/kom</b><br />
						za sve kokošje pasmine i fazane.<br />
						<b>1.5 kn/kom</b><br />
						za pure<br />
						<b>1.5 kn/kom</b><br />
						za bijele patke (domaće patke ne ležemo!)<br /></p>
					</div>

					<div id="pilici">
						<h3>Pilići</h3><br />
						<p><b>6 kn/kom</b><br /><br />
						Dostavljamo na području Požeško-slavonske<br/> i dio Brodsko-posavske županije.<br /></p>
					</div>
				</div>				
			</div>

			<div id="rezervacija" class="content float">
				<div id="datepicker"></div>
				<div id="datepicker2"></div>
				<?php
					require('assets/php/funkcije.php');
					if($_POST){
						$option = $_POST['skriveno'];
						if($option==1){
							$broj_pilica = $_POST['broj_pilica'];
							$datum = $_POST['datum'];
							$ime_prezime = $_POST['ime_prezime'];
							$kontakt_telefon = $_POST['kontakt_telefon'];
							$adresa = $_POST['adresa'];
							$kucni_broj = $_POST['kucni_broj'];
							$mjesto = $_POST['mjesto'];

							if($broj_pilica && $datum && $ime_prezime && $kontakt_telefon && $adresa && $kucni_broj && $mjesto){
								if(empty($_POST['email']))
									$email = 'N/A';
								else
									$email = $_POST['email'];
							$c=db();
							$upit="INSERT INTO rezervacija_pilici(broj_pilica,datum,ime_prezime,kontakt_telefon,adresa,kucni_broj,mjesto,email) VALUES('$broj_pilica','$datum','$ime_prezime','$kontakt_telefon','$adresa','$kucni_broj','$mjesto','$email')";
							$c->query($upit);
							$c->close();
							}
						}
						else if($option==2){
							$datum1 = $_POST['datum1'];
							$ime_prezime = $_POST['ime_prezime'];
							$kontakt_telefon = $_POST['kontakt_telefon'];
							if($datum1 && $ime_prezime && $kontakt_telefon){
								if(empty($_POST['email']))
									$email = 'N/A';
								else
									$email = $_POST['email'];
									$jaja_kokos = 0;
									$jaja_bijela_patka = 0;
									$jaja_pura = 0;
									$jaja_fazan = 0;

								if(!empty($_POST['kokos']))
									$jaja_kokos = $_POST['jaja_kokos'];
								if(!empty($_POST['bijela_patka']))
									$jaja_bijela_patka = $_POST['jaja_bijela_patka'];
								if(!empty($_POST['pura']))
									$jaja_pura = $_POST['jaja_pura'];
								if(!empty($_POST['fazan']))
									$jaja_fazan = $_POST['jaja_fazan'];

								$c=db();
								$upit="INSERT INTO rezervacija_inkubator(datum,jaja_kokos,jaja_bijela_patka,jaja_pura,jaja_fazan,ime_prezime,kontakt_telefon,email) VALUES('$datum1','$jaja_kokos','$jaja_bijela_patka','$jaja_pura','$jaja_fazan','$ime_prezime','$kontakt_telefon','$email')";
								$c->query($upit);
								$c->close();
							}
						}
					}				
				?>
				<div id="rezervacija-pilica">
					<form name="rezervacija_pilici" method="post" action="">
						<p>Broj pilica: &nbsp &nbsp &nbsp &nbsp
							<input class ="text" name="broj_pilica" type="text" id="broj_pilica" maxlength="5" size="5">
						</p>
						<p>Zeljeni datum: &nbsp &nbsp &nbsp &nbsp
							<input class ="text" name="datum" type="text" id="datum">
						</p><br />
						<p>Ime i prezime: &nbsp &nbsp &nbsp &nbsp
							<input class ="text" name="ime_prezime" type="text" id="ime_prezime" maxlength="30" size="30">
						</p>
						<p>Kontakt telefon: &nbsp &nbsp &nbsp &nbsp
							<input class ="text" name="kontakt_telefon" type="text" id="kontakt_telefon" maxlength="15">
						</p>
						<p>Adresa: &nbsp &nbsp &nbsp &nbsp
							<input class ="text" name="adresa" type="text" id="adresa" maxlength="40" size="40">
						</p>
						<p>Kucni broj: &nbsp &nbsp &nbsp &nbsp
							<input class ="text" name="kucni_broj" type="text" id="kucni_broj" maxlength="3" size="5">
						</p>
						<p>Mjesto: &nbsp &nbsp &nbsp &nbsp
							<input class ="text" name="mjesto" type="text" id="mjesto" maxlength="30" size="30">
						</p>
						<p>E-mail: &nbsp &nbsp &nbsp &nbsp
							<input class ="text" name="email" type="text" id="email" maxlength="30" size="30">
						</p>
						<p>
							<input name="skriveno" type="hidden" id="skriveno" value="1">
							<input class ="button-pilici" type="submit" name="Submit" value="Pošalji rezervaciju">
						</p>
					</form>
				</div>

				<div id="rezervacija-inkubatora">
					<h2>Vrste jaja</h2><br />
					<form name="rezervacija_inkubator" method="post" action="">
						<div id="rez-wrapper" class="float">
							<div id="rez-kokos"> 
								<p> 
							    <input class="chck" type="checkbox" name="kokos" value="1">
							    Kokoš <br>
							    Broj jaja 
							    <input class ="text-sp" name="jaja_kokos" type="text" id="jaja_kokos" maxlength="3" size="5">
							    </p>
						    </div>
							<div id="rez-pura">
								 <p> 
								 <input class="chck" type="checkbox" name="pura" value="3">
								 Pura<br>
								 Broj jaja 
								 <input class ="text-sp" name="jaja_pura" type="text" id="jaja_pura" maxlength="3" size="5">
								 </p>
							</div>
							<div id="rez-bpatka">
								<p> 
							    <input class="chck" type="checkbox" name="bijela_patka" value="2">
							    Bijela patka<br>
							    Broj jaja 
							    <input class ="text-sp" name="jaja_bijela_patka" type="text" id="jaja_bijela_patka" maxlength="3" size="5">
							  	</p> 
							</div>
							<div id="rez-fazan">
								<p> 
							    <input class="chck" type="checkbox" name="fazan" value="4">
							    Fazan<br>
							    Broj jaja 
							    <input class ="text-sp" name="jaja_fazan" type="text" id="jaja_fazan" maxlength="3" size="5">
						  		</p>
							</div>

						</div>
						
						<br /><p>Zeljeni datum 					
							<input class ="text" name="datum1" type="text" id="datum1">
						</p>
						<p>Ime i prezime 
							<input class ="text" name="ime_prezime" type="text" id="ime_prezime" maxlength="30" size="30">
						</p>
						<p>Kontakt telefon 
							<input class ="text" name="kontakt_telefon" type="text" id="kontakt_telefon" maxlength="15">
						</p>
						<p>E-mail 
							<input class ="text" name="email" type="text" id="email" maxlength="30" size="30">
						</p>
						<p>
							<input name="skriveno" type="hidden" id="skriveno" value="2">
							<input class ="button-pilici" type="submit" name="Submit2" value="Pošalji rezervaciju">
						</p>
					</form>

				</div>					
				
			</div>		

			<div id="kontakt" class="content">
				<div id="kontakt-txt">					
					<p>Loman d.o.o.<br />
					Kralja Zvonimira 59<br />
					Pleternica 34310 <br /><br />

					Tel: 034-251-788<br />
					Mobitel: 098-1764-441<br />
					Email: pktaus@gmail.com</p>
				</div>

				<div id="map-canvas">
					<!--google mapa-->
				</div>

			</div>

			<footer>
				<p>Izradili: Maja Grubišić, Tina Maršić, Robert Užar i Petar Živković | Odjel za informatiku Sveučilišta u Rijeci</p>
			</footer>

		</div>
	</body>
</html>