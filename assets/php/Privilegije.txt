
-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 15, 2013 at 03:23 PM
-- Server version: 5.5.16
-- PHP Version: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `loman_upis_pilica`
--
CREATE DATABASE `loman_upis_pilica` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `loman_upis_pilica`;

-- --------------------------------------------------------

--
-- Table structure for table `rezervacija_inkubator`
--

CREATE TABLE IF NOT EXISTS `rezervacija_inkubator` (
  `id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `datum` date NOT NULL,
  `jaja_kokos` int(3) unsigned NOT NULL,
  `jaja_bijela_patka` int(3) unsigned NOT NULL,
  `jaja_pura` int(3) unsigned NOT NULL,
  `jaja_fazan` int(3) unsigned NOT NULL,
  `ime_prezime` varchar(30) NOT NULL,
  `kontakt_telefon` varchar(15) NOT NULL,
  `email` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Table structure for table `rezervacija_pilici`
--

CREATE TABLE IF NOT EXISTS `rezervacija_pilici` (
  `id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `broj_pilica` int(5) unsigned NOT NULL,
  `datum` date NOT NULL,
  `ime_prezime` varchar(30) NOT NULL,
  `kontakt_telefon` varchar(15) NOT NULL,
  `adresa` varchar(40) NOT NULL,
  `kucni_broj` int(3) unsigned NOT NULL,
  `mjesto` varchar(30) NOT NULL,
  `email` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;



CREATE USER 'kresimir'@'localhost' IDENTIFIED BY 'kresimir';

GRANT USAGE ON * . * TO 'kresimir'@'localhost' IDENTIFIED BY '***' WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0 ;

GRANT ALL PRIVILEGES ON `loman_upis_pilica` . * TO 'kresimir'@'localhost';

FLUSH PRIVILEGES;