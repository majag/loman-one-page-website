jQuery(function($){
	// Navigacija
	$('#nav').onePageNav({
	   currentClass: 'current',
	   changeHash: false,
	   scrollSpeed: 1200
	});

	// Slider
	var Page = (function() {
		var $nav = $( '#nav-dots > span' ),
		slitslider = $( '#slider' ).slitslider( {
			onBeforeChange : function( slide, pos ) {
				$nav.removeClass( 'nav-dot-current' );
				$nav.eq( pos ).addClass( 'nav-dot-current' );
			},
			autoplay:true,
		} ),	

		init = function() {
			initEvents();
		},
		initEvents = function() {
			$nav.each( function( i ) {
				$( this ).on( 'click', function( event ) {
					var $dot = $( this );
					if( !slitslider.isActive() ) {
						$nav.removeClass( 'nav-dot-current' );
						$dot.addClass( 'nav-dot-current' );
					}

					slitslider.jump( i + 1 );
					return false;
				} );
			} );

		};

		return { init : init };

	})();
	Page.init();
});