function initialize(){
				var Mycenter = new google.maps.LatLng(45.290656,17.806746);				
				var styles = [
					{
						featureType: 'landscape',
						elementType: 'all',
						stylers: [
							{ hue: '#ffefdf' },
							{ saturation: 100 },
							{ lightness: 43 },
							{ visibility: 'on' }
						]
					},{
						featureType: 'road',
						elementType: 'labels',
						stylers: [
							{ hue: '#f19f4a' },
							{ saturation: -14 },
							{ lightness: -3 },
							{ visibility: 'simplified' }
						]
					},{
						featureType: 'road',
						elementType: 'geometry',
						stylers: [
							{ hue: '#db9624' },
							{ saturation: -28 },
							{ lightness: -22 },
							{ visibility: 'on' }
						]
					},{
						featureType: 'road',
						elementType: 'labels',
						stylers: [
							{ hue: '#e6d2b2' },
							{ saturation: -49 },
							{ lightness: 44 },
							{ visibility: 'on' }
						]
					}
				];
				var options = {					
					center: new google.maps.LatLng(45.290656, 17.806746),
					zoom: 15,
					mapTypeId: 'Styled'
				};
				var div = document.getElementById('map-canvas');
				var map = new google.maps.Map(div, options);
				var styledMapType = new google.maps.StyledMapType(styles);
				map.mapTypes.set('Styled', styledMapType);
				var marker=new google.maps.Marker({
					position:Mycenter,					
					});
				marker.setMap(map);
}
google.maps.event.addDomListener(window, 'load', initialize);
